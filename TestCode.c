#include <stdlib.h>
#include <string.h>
#include "TestCode.h"
/*
# Cloning
When cloning this repo, use `git clone --recurse-submodules` so that it automatically pulls the Googletest repo.
If the repo was not cloned this way, it can be updated after the fact by running `git submodule update --init`

# Objective
Implement a [hash table](https://en.wikipedia.org/wiki/Hash_table) with the following requirements:
1. The hash table's size is fixed, but specified at initialization time
   * Use `hashtable_init(size)`
   * Table must be dynamically allocated at runtime
1. Use the supplied hash function to compute the index
   * Use `djb2_hash(string)`
1. When inserting items into the hash table, the strings must be copied. The provided input cannot be assumed to be long-lived.
   * Items will be provided as `char*` keys and values
   * Use `hashtable_insert(table, key, value)`
1. Collisions should be handled using the "separate chaining" technique of using linked lists for each entry
   * Linked list code is not provided
1. When inserting a key that already exists, the value should be updated.
   * Multiple inserts using the same key should not be treated as a collision
1. Data is retrieved from the hash table using `hashtable_get(table, key)`
   * If a key does not exist in the table, return `NULL`
1. The hash table does not need to support removing entries
1. The hash table can be destroyed using `hashtable_free(table)`
   * All allocated memory must be released
1. Include error handling. For failures, return `NULL` when pointers are expected and `false` when booleans are expected.

# Building
This repo is set up for CMake and is compatible with VSCode and the CMake Tools extension if desired.

To build the project and run the tests, use the following commands:
```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
./TestCode
```

This repo uses the built-in GCC analyzers to help identify memory faults and leaks. Leak detection interferes with GDB,
so it is disabled in debug mode. To verify that there are no leaks, also build and test in release mode:
```
cd build
rm -rf *
cmake -DCMAKE_BUILD_TYPE=Release ..
make
./TestCode
```

The initial build will take a bit longer since it needs to build the googletest library first.
*/


/**
 * Simple hashing function for ASCII strings
 * @author D. J. Bernstein
 * @copyright 1991
 * 
 * @param str Null terminated character array
 * @return hash of string
 */
unsigned long djb2_hash(char *str)
{
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

/**
 * Initializes a hash table to the given size
 * 
 * @param size Number of entries in the hash table
 * 
 * @return Pointer to the new hash table
 */
hashtable* hashtable_init(int size)
{
	//Sanity check, else unit test will fail
	if (size == 0){
		return NULL;
	}

	//Allocate heap for hash Table
 	hashtable * ht = malloc(sizeof(hashtable));

	//Sanity check! 
	if(!ht){
		return NULL;
	}

	//Allocate heap for elem table and sanity check
	if((ht->table = malloc(size*sizeof(hashElem*))) == NULL)
	{
		free(ht);
		return NULL;
	}

	/*
	Initialize table per given size and default all others
	*/
	ht->size = size;
	ht->elemSize = 0;
	unsigned int i;
	for(i = 0; i < size; i++)
		ht->table[i] = NULL; //empty table
	return ht;
}

/**
 * Inserts a (key, value) pair into the hash table
 * 
 * @param table The hash table to add the entry to
 * @param key The key to store the value at
 * @param value The value to store
 * 
 * @return Success or failure of inserting the value
 */
bool hashtable_insert(hashtable* table, const char* key, const char* value)
{
	//Sanity checks, else unit test will fail!
    if ( (NULL == value) || (NULL == key) ){
        return false;
    }

    int keyLen = strlen(key);
    int valueLen = strlen(value);
	unsigned int h = djb2_hash(key) % table->size;
	hashElem* e = table->table[h];

	/*
	Existing elem with key? If so, update accordingly
	*/
	while(e != NULL)
	{
		if(!strcmp(e->key, key))
		{
			void* ret = e->value;
			e->value = value;
			return true;
		}
		e = e->next;
	}

	//Okay, so time to insert new elem
	if((e = malloc(sizeof(hashElem)+keyLen+1)) == NULL)
		return false;
	strcpy(e->key, key);//Copy key
    e->value = value;

	//Then updating the list and table accordingly
	//As well as the table size 
	e->next = table->table[h];
	table->table[h] = e;
	table->elemSize ++;
    
    return true;
}

/**
 * Retrieves a value from the hash table
 * 
 * @param table The hash table to operate on
 * @param key The key to retrieve the value from
 * 
 * @return The value
 */
char* hashtable_get(hashtable* table, const char* key)
{
	//Sanity check else unit test will fail!
	if (NULL == key){
		return NULL;
	}

	int keyLen = strlen(key);
 
	//Get hash
 	unsigned int h = djb2_hash(key) % table->size;
	hashElem* e = table->table[h];
	/*
	Locate elem per given key
	*/
	while(e != NULL)
	{
		if(!strcmp(e->key, key))
			return e->value;//Found elem!!!
		e = e->next;
	}
    //Not found
	return NULL;
}

/* 	
Given a table and a key, key free() the associated elem 
 */
void htFreeElem(hashtable * table, char* key)
{
	//Get hash 
	unsigned int h = djb2_hash(key) % table->size;

	//Initial conditions
	hashElem* e = table->table[h];
	hashElem* prev = NULL;
	while(e != NULL)
	{
		/*
		Found matched key?
		*/
		if(strcmp(e->key, key) == 0)
		{
			if(prev != NULL)
				prev->next = e->next;
			else
				table->table[h] = e->next;
			free(e);
			e = NULL;//Safety measure!
			table->elemSize --;//Decrement elem size accordingly
			break;
		}
		prev = e;
		e = e->next;
	}
}

/**
 * Frees all memory associated with the given hash table
 */
void hashtable_free(hashtable* table)
{
	// Sanity check
	if (NULL == table){
		return;
	}

	char * tmpKey = NULL;
	hashElem* e;
 	elemIter it = {table, 0, table->table[0]}; 
	elemIter *itPtr = &it;
	while(itPtr->currentElem == NULL)
	{
		/*
		Are we gone through hash table capacity?
		If not, get the next table
		*/
		if(itPtr->currentIndex < itPtr->currentHt->size - 1)
		{
			itPtr->currentIndex++;
			itPtr->currentElem = itPtr->currentHt->table[itPtr->currentIndex];
		}
		else
		 	break; //No more
	}
	//Set for next run
	e = itPtr->currentElem;
	if(e){
		itPtr->currentElem = e->next; 
	}	

	//Sanity condition
	if (NULL != e){
		tmpKey = e->key;
	}
	
	/*
	Iterate through table and free elem accordingly
	*/
	while(tmpKey != NULL)
	{
		/*
		Free elem
		*/
		htFreeElem(table, tmpKey); 

		//e = ht_iterate(&it);
		while(itPtr->currentElem == NULL)
		{
			if(itPtr->currentIndex < itPtr->currentHt->size - 1)
			{
				itPtr->currentIndex++;
				itPtr->currentElem = itPtr->currentHt->table[itPtr->currentIndex];
			}
			else
				break;
		}
		e = itPtr->currentElem;
		if(e){
			itPtr->currentElem = e->next; 
		}	

		
		if (NULL == e){
			tmpKey = NULL;
		}else{
			tmpKey = e->key;
		}
	}

	/*
	Free the remaining structures, else get memory leaks!!!
	*/
    free(table->table);
    free(table);

}
