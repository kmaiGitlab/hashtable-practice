#pragma once
#include <stdbool.h>

//For hash element as linkedlist as required
typedef struct hashElem {
	struct hashElem* next; //Linkedlist used for seperate chaining
	void* value;//Pointer to the stored value
	char key[];//Key
} hashElem;

//Hash Table structure
typedef struct hashtable_ {
	unsigned int size;	//Per user given size 
	unsigned int elemSize;//Number of elements in the table 
	hashElem** table;	
} hashtable;

//Support structure containing current iteration data while freeing memory
typedef struct {
	hashtable * currentHt; 	
	unsigned int currentIndex;	
	hashElem* currentElem; 	
} elemIter;


#ifdef __cplusplus
extern "C" {
#endif
    // Hash table functions
    hashtable* hashtable_init(int size);
    bool hashtable_insert(hashtable* table, const char* key, const char* value);
    char* hashtable_get(hashtable* table, const char* key);
    void hashtable_free(hashtable* table);

    // Linked list functions

#ifdef __cplusplus
}
#endif
