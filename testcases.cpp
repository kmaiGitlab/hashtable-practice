#include <gtest/gtest.h>
#include "TestCode.h"

TEST(HashTest, NormalUsage)
{
    hashtable* table = hashtable_init(5);
    ASSERT_NE(table, nullptr);

    EXPECT_TRUE(hashtable_insert(table, "dog", "woof"));
    EXPECT_TRUE(hashtable_insert(table, "cat", "meow"));
    EXPECT_TRUE(hashtable_insert(table, "bird", "chirp"));
    EXPECT_TRUE(hashtable_insert(table, "mouse", "squeek"));
    EXPECT_TRUE(hashtable_insert(table, "velociraptor", "ROAR"));
    EXPECT_TRUE(hashtable_insert(table, "cow", "moo"));

    EXPECT_STREQ(hashtable_get(table, "dog"), "woof");
    EXPECT_STREQ(hashtable_get(table, "cat"), "meow");
    EXPECT_STREQ(hashtable_get(table, "bird"), "chirp");
    EXPECT_STREQ(hashtable_get(table, "mouse"), "squeek");
    EXPECT_STREQ(hashtable_get(table, "velociraptor"), "ROAR");
    EXPECT_STREQ(hashtable_get(table, "cow"), "moo");

    hashtable_free(table);
}

TEST(HashTest, Overwrite)
{
    hashtable* table = hashtable_init(5);
    ASSERT_NE(table, nullptr);

    EXPECT_TRUE(hashtable_insert(table, "dog", "woof"));
    EXPECT_TRUE(hashtable_insert(table, "cat", "meow"));
    EXPECT_TRUE(hashtable_insert(table, "bird", "chirp"));
    EXPECT_TRUE(hashtable_insert(table, "mouse", "squeek"));
    EXPECT_TRUE(hashtable_insert(table, "velociraptor", "ROAR"));
    EXPECT_TRUE(hashtable_insert(table, "cow", "moo"));

    EXPECT_TRUE(hashtable_insert(table, "dog", "bark"));
    EXPECT_TRUE(hashtable_insert(table, "cat", "hiss"));
    EXPECT_TRUE(hashtable_insert(table, "bird", "tweet"));
    EXPECT_TRUE(hashtable_insert(table, "mouse", "scritch"));
    EXPECT_TRUE(hashtable_insert(table, "velociraptor", "CAAAAAA"));

    EXPECT_STREQ(hashtable_get(table, "dog"), "bark");
    EXPECT_STREQ(hashtable_get(table, "cat"), "hiss");
    EXPECT_STREQ(hashtable_get(table, "bird"), "tweet");
    EXPECT_STREQ(hashtable_get(table, "mouse"), "scritch");
    EXPECT_STREQ(hashtable_get(table, "velociraptor"), "CAAAAAA");

    hashtable_free(table);
}

TEST(HashTest, ErrorHandling)
{
    hashtable* table = hashtable_init(0);
    EXPECT_EQ(table, nullptr);

    table = hashtable_init(5);
    ASSERT_NE(table, nullptr);

    EXPECT_FALSE(hashtable_insert(table, "dog", nullptr));
    EXPECT_FALSE(hashtable_insert(table, nullptr, "woof"));
    EXPECT_FALSE(hashtable_insert(nullptr, nullptr, nullptr));

    EXPECT_TRUE(hashtable_insert(table, "dog", "woof"));
    EXPECT_EQ(hashtable_get(table, "cat"), nullptr);
    EXPECT_EQ(hashtable_get(table, nullptr), nullptr);

    hashtable_free(nullptr);
    hashtable_free(table);
}